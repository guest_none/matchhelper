﻿//=============================================================================
/// @file Main window UI - Implementation
/// @author guest_none (Patrick Rećko) <guest_none@protonmail.com>
//=============================================================================
//  Copyright (C) Patrick Rećko, all rights reserved.
//  This source file is released under MirOS License
//=============================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Ookii.Dialogs.Wpf;
using MatchHelper.Core;
using NiceIO;
using System.Collections.ObjectModel;

namespace MatchHelper
{
   /// <summary>
   /// Interaction logic for MainWindow.xaml
   /// </summary>
   public partial class MainWindow : Window {
      private class CompoInfoTmp {
         public string c
         {
            get;
            set;
         }

         public string s
         {
            get;
            set;
         }

         public CompoInfoTmp(CompetitionType c, StageType s)
         {
            this.c = UIUtils.competitionTypeToString(c);
            this.s = UIUtils.stageTypeToString(s);
         }

      }

      private class ResultsInfoTmp {
         public string c
         {
            get;
            set;
         }

         public string s
         {
            get;
            set;
         }

         public string w
         {
            get;
            set;
         }

         public ResultsInfoTmp(CompetitionType c, StageType s, string winner)
         {
            this.c = UIUtils.competitionTypeToString(c);
            this.s = UIUtils.stageTypeToString(s);
            this.w = winner;
         }
      }

      private ObservableCollection<CompoInfoTmp> c_list = new ObservableCollection<CompoInfoTmp>();
      private ObservableCollection<ResultsInfoTmp> r_list = new ObservableCollection<ResultsInfoTmp>();

      public MainWindow()
      {
         InitializeComponent();
         compolist.ItemsSource = c_list;
         resultslist.ItemsSource = r_list;
      }

      private void saveSys_Click(object sender, RoutedEventArgs e)
      {
         VistaFolderBrowserDialog d = new VistaFolderBrowserDialog();
         d.ShowDialog();
         if (d.SelectedPath != "")
            MatchHelper.App.g_mgr.saveSystemState(new NPath(d.SelectedPath));
      }

      private void loadSys_Click(object sender, RoutedEventArgs e)
      {
         if (MessageBox.Show("Are you sure do you want load?\nUnsaved changes would be lost", "Load", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes) {
            VistaFolderBrowserDialog d = new VistaFolderBrowserDialog();
            d.ShowDialog();
            if (d.SelectedPath != "")
               MatchHelper.App.g_mgr.loadSystemState(new NPath(d.SelectedPath));
            regenerateCompoList();
            regenerateResultsList();
         } else {

         }
         
      }

      private void about_Click(object sender, RoutedEventArgs e)
      {
         About a = new About();
         a.Show();
      }

      private void exit_Click(object sender, RoutedEventArgs e)
      {
         if (MessageBox.Show("Are you sure do you want to exit?\nUnsaved changes would be lost", "Exit", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes) {
            System.Environment.Exit(1);
         } else {

         }
      }

      private void regenerateCompoList()
      {
         c_list = new ObservableCollection<CompoInfoTmp>();
         foreach (var compo in App.g_mgr.getCompetitionList()) {
            c_list.Add(new CompoInfoTmp(compo.getCompoType(), compo.getStageType()));
         }
         compolist.ItemsSource = c_list;
      }

      private void regenerateResultsList()
      {
         r_list = new ObservableCollection<ResultsInfoTmp>();
         foreach (var result in App.g_mgr.getResultsList()) {
            r_list.Add(new ResultsInfoTmp(result.getCompoType(), result.getStageType(), result.getWinner().getTeam().getTeamName()));
         }
         resultslist.ItemsSource = r_list;

      }

      private void compolist_MouseDoubleClick(object sender, RoutedEventArgs e)
      {
         var selidx = compolist.SelectedIndex;
         if (selidx != -1) {
            if (App.g_mgr.getCompetitionList().Count != 0) {
               CompoWindow w = new CompoWindow(selidx);
               w.ShowDialog();
            } else {
               // Do nothing
            }
         }
         regenerateCompoList();
         regenerateResultsList();
      }



      private void resultslist_MouseDoubleClick(object sender, RoutedEventArgs e)
      {
         var selidx = resultslist.SelectedIndex;
         if (selidx != -1) {
            if (App.g_mgr.getResultsList().Count != 0) {
               var rwnd = new ResultsWindow(App.g_mgr.getResultsList()[selidx]);
               rwnd.ShowDialog();
            } else {
               // Do nothing
            }
         }
         regenerateCompoList();
         regenerateResultsList();
      }

      private void addcompo_Click(object sender, RoutedEventArgs e)
      {
         AddCompoWindow w = new AddCompoWindow();
         w.ShowDialog();
         regenerateCompoList();
      }

      private void deletecompoContext_Click(object sender, RoutedEventArgs e)
      {
         var selidx = compolist.SelectedIndex;
         if (selidx != -1) {
            if (App.g_mgr.getCompetitionList().Count != 0) {
               if (MessageBox.Show("Do you want to delete?", "Question", MessageBoxButton.YesNo) == MessageBoxResult.Yes) {
                  App.g_mgr.competition_storage.RemoveAt(selidx);
                  regenerateCompoList();
               }
            } else {
               // Do nothing
            }
         }
         regenerateCompoList();
      }

      private void deleteresultsContext_Click(object sender, RoutedEventArgs e)
      {
         var selidx = resultslist.SelectedIndex;
         if (selidx != -1) {
            if (App.g_mgr.getResultsList().Count != 0) {
               if (MessageBox.Show("Do you want to delete?", "Question", MessageBoxButton.YesNo) == MessageBoxResult.Yes) {
                  App.g_mgr.results_storage.RemoveAt(selidx);
                  regenerateResultsList();
               }
            } else {
               // Do nothing
            }
         }
         regenerateCompoList();
      }

   }
}

