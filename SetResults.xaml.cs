﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using MatchHelper.Core;
using MatchHelper;

namespace MatchHelper {
   /// <summary>
   /// Interaction logic for SetResults.xaml
   /// </summary>
   public partial class SetResults : Window {

      private Core.Match _m;

      public SetResults()
      {
         InitializeComponent();
      }

      public SetResults(Core.Match m)
      {
         InitializeComponent();
         this._m = m;
      }

      private void onlyNumberAllowed(object sender, TextCompositionEventArgs e)
      {
         Regex regex = new Regex("^[^0-9\\s]+$"); //regex that matches disallowed text
         e.Handled = regex.IsMatch(e.Text);
      }

      private void button_Click(object sender, RoutedEventArgs e)
      {
         _m.setResults(Convert.ToInt16(rawfirsttextBox.Text), Convert.ToInt16(this.rawsecondtextBox.Text), 0, 0);
         this.Close();
      }

      private void button1_Click(object sender, RoutedEventArgs e)
      {
         this.Close();
      }
   }
}
