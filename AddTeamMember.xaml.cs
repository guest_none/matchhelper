﻿//=============================================================================
/// @file Add team
/// @author guest_none (Patrick Rećko) <guest_none@protonmail.com>
//=============================================================================
//  Copyright (C) Patrick Rećko, all rights reserved.
//  This source file is released under MirOS License
//=============================================================================


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MatchHelper.Core;

namespace MatchHelper {
   /// <summary>
   /// Interaction logic for AddTeamMember.xaml
   /// </summary>
   public partial class AddTeamMember : Window {

      public TeamMember t
      {
         get;
         private set;
      }

      private Team _t;
      public bool to_add = false;

      public AddTeamMember()
      {
         InitializeComponent();
      }
      
      public AddTeamMember(Team t)
      {
         InitializeComponent();
         this._t = t;
      }

      private TeamMemberType checkBoxToTeamMemberType()
      {
         if (this.iscaptaincheckBox.IsChecked.Value == true) {
            return TeamMemberType.captain;
         } else {
            return TeamMemberType.normal;
         }
      }

      private void addButton_Click(object sender, RoutedEventArgs e)
      {
         if (firsttextBox.Text == "" || lasttextBox.Text == "") {
            MessageBox.Show("Please type your data", "Error", MessageBoxButton.OK);
         } else {
            if (_t.isCaptainAdded() == this.iscaptaincheckBox.IsChecked.Value) {
               MessageBox.Show("Captain allready added", "Error", MessageBoxButton.OK);
            } else {
               try {
                  _t.addMember(new TeamMember(firsttextBox.Text, lasttextBox.Text, checkBoxToTeamMemberType()));
               } catch (TooManyMembersExcept ex) {
                  MessageBox.Show("Added too many members", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
               } finally {
                  this.Close();
               }
            }
         }
      }

      private void cancelButton_Click(object sender, RoutedEventArgs e)
      {
         this.Close();
      }
   }
}
