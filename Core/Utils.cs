﻿//=============================================================================
/// @file Utility routines
/// @author guest_none (Patrick Rećko) <guest_none@protonmail.com>
//=============================================================================
//  Copyright (C) Patrick Rećko, all rights reserved.
//  This source file is released under MirOS License
//=============================================================================

using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// This class extends generics to support randomized sellections
/// Code taken from https://stackoverflow.com/questions/2019417/access-random-item-in-list
/// Released under whatever the hell Stack Overflow licenses code snippets
/// (I heard that is MIT License, probably)
/// </summary>
public static class EnumerableExtension {
   public static T PickRandom<T>(this IEnumerable<T> source)
   {
      return source.PickRandom(1).Single();
   }

   public static IEnumerable<T> PickRandom<T>(this IEnumerable<T> source, int count)
   {
      return source.Shuffle().Take(count);
   }

   public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
   {
      return source.OrderBy(x => Guid.NewGuid());
   }
}

class ReverseIntComparer : IComparer<int> {
   public int Compare(int x, int y)
   {
      return -x.CompareTo(y);
   }
}

/// <summary>
/// Comparer for comparing two keys, handling equality as beeing greater
/// Use this Comparer e.g. with SortedLists or SortedDictionaries, that don't allow duplicate keys
/// </summary>
/// <typeparam name="TKey"></typeparam>
public class DuplicateKeyComparer<TKey>
                :
             IComparer<TKey> where TKey : IComparable {
   #region IComparer<TKey> Members

   public int Compare(TKey x, TKey y)
   {
      int result = x.CompareTo(y);

      if (result == 0)
         return 1;   // Handle equality as beeing greater
      else
         return result;
   }

   #endregion
}