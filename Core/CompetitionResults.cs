﻿//=============================================================================
/// @file Competition results routines
/// @author guest_none (Patrick Rećko) <guest_none@protonmail.com>
//=============================================================================
//  Copyright (C) Patrick Rećko, all rights reserved.
//  This source file is released under MirOS License
//=============================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatchHelper.Core {
   /// <summary>
   /// Stores results of the competition and it's classification
   /// </summary>
   public class CompetitionResults {

      #region Private functions and structures

      public StageType stage_type;
      public CompetitionType compo_type;
      public SortedList<int, Place> classification;

      #endregion // Private functions and structures

      #region Public functions

      /// <summary>
      /// Creates a new object of this type
      /// </summary>
      /// <param name="stage_type"> Type of stage</param>
      /// <param name="compo_type"> Type of competition</param>
      /// <param name="winner">The winner for this stage</param>
      /// <param name="classification"></param>
      public CompetitionResults(StageType stage_type,
                                CompetitionType compo_type,
                                Team winner,
                                SortedList<int, Place> classification)
      {
         this.stage_type = stage_type;
         this.compo_type = compo_type;
         this.classification = classification;
      }

      public Place getWinner()
      {
         return classification[1];
      }

      public SortedList<int, Place> getClassification()
      {
         return this.classification;
      }

      public StageType getStageType()
      {
         return this.stage_type;
      }

      public CompetitionType getCompoType()
      {
         return this.compo_type;
      }

      #endregion // Public functions 
   }

   /// <summary>
   /// This class is responsible for storing informations about 
   /// the team's place and it's number of points
   /// </summary>
   public class Place {

      #region Private functions and structures

      public Team team;
      public int place;
      public int num_of_points;

      #endregion // Private functions and structures

      #region Public functions

      public Place(Team team,
                   int place,
                   int points)
      {
         this.team = team;
         this.place = place;
         this.num_of_points = points;
      }

      public Team getTeam()
      {
         return this.team;
      }

      public int getPlace()
      {
         return this.place;
      }

      public int getPoints()
      {
         return this.num_of_points;
      }

      #endregion // Public functions 
   }
}
