﻿//=============================================================================
/// @file Team routines
/// @author guest_none (Patrick Rećko) <guest_none@protonmail.com>
//=============================================================================
//  Copyright (C) Patrick Rećko, all rights reserved.
//  This source file is released under MirOS License
//=============================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatchHelper.Core {
   public enum TeamMemberType {
      captain,
      normal
   };

   /// <summary>
   /// Stores informations about the team and it's membera
   /// </summary>
   public class Team {
      public string name = "Examples";
      public int size = 2;
      public List<TeamMember> member_list = new List<TeamMember>();
      public TeamMember captain;
      public bool is_captain_added = false;

      #region Public functions

      /// <summary>
      /// Creates a new team object
      /// </summary>
      /// <param name="name">Name of the team</param>
      /// <param name="size">Size of the team</param>
      public Team(string name, int size)
      {
         this.name = name;
         this.size = size;
      }

      public void addMember(TeamMember member)
      {
         if (this.member_list.Count() >= this.size) {
            throw new TooManyMembersExcept("Too many members added:");
         } else {
            this.member_list.Add(member);
         }

         if (member.getMemberType() == TeamMemberType.captain) {
            this.captain = member;
            this.is_captain_added = true;
         }
      }

      public string getTeamName()
      {
         return this.name;
      }

      public int getTeamSize()
      {
         return this.size;
      }

      public bool isCaptainAdded()
      {
         return this.is_captain_added;
      }

      public TeamMember getCaptain()
      {
         if (this.is_captain_added == false) {
            throw new NoCaptainExcept("No captain added!");
         } else {
            return captain;
         }
      }

      public List<TeamMember> getMemberList()
      {
         return this.member_list;
      }


      #endregion // Public functions 
   }
   
   /// <summary>
   /// Stores informations about the team member
   /// </summary>
   public class TeamMember {
      
      #region Private functions and structures

      public string first_name = "Jacob";
      public string last_name = "Kowalsky";
      public TeamMemberType type = TeamMemberType.normal;
      
      #endregion // Private functions and structures

      #region Public functions

      /// <summary>
      /// Initialize a new Team member
      /// </summary>
      /// <param name="first">First name</param>
      /// <param name="last">Last name</param>
      /// <param name="type">Type of the member (either captain or normal)</param>
      public TeamMember(string first, string last, TeamMemberType type)
      {
         this.first_name = first;
         this.last_name = last;
         this.type = type;
      }
      
      public string getFirstName()
      {
         return this.first_name;
      }

      public string getLastName()
      {
         return this.last_name;
      }

      public TeamMemberType getMemberType()
      {
         return this.type;
      }
      
      #endregion // Public functions     

   }

   class TooManyMembersExcept : Exception {
      public TooManyMembersExcept()
      {
      }

      public TooManyMembersExcept(string msg) : base(msg)
      {
      }
   }

   class NoCaptainExcept : Exception {
      public NoCaptainExcept()
      {
      }

      public NoCaptainExcept(string msg) : base(msg)
      {
      }
    }

   
}
