﻿//=============================================================================
/// @file Referee routines
/// @author guest_none (Patrick Rećko) <guest_none@protonmail.com>
//=============================================================================
//  Copyright (C) Patrick Rećko, all rights reserved.
//  This source file is released under MirOS License
//=============================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatchHelper.Core {

   /// <summary>
   /// This class stores information about the referee
   /// </summary>
   public class Referee {

      #region Private functions and structures

      public string first_name;
      public string last_name;
      /// <summary>
      /// Stores the list of disciplines in which the referee
      /// can judge
      /// </summary>
      public List<CompetitionType> speciality;

      #endregion // Private functions and structures

      #region Public functions

      /// <summary>
      /// Creates a new Referee object
      /// </summary>
      /// <param name="first">First name</param>
      /// <param name="last">L:ast name</param>
      /// <param name="spec">List of specialities</param>
      public Referee(string first, string last, List<CompetitionType> spec)
      {
         this.first_name = first;
         this.last_name = last;
         this.speciality = spec;
      }

      public string getFirstName()
      {
         return this.first_name;
      }

      public string getLastName()
      {
         return this.last_name;
      }

      public List<CompetitionType> getSpeciality()
      {
         return this.speciality;
      }

      #endregion // Public functions 
   }
}
