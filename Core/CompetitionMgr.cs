﻿//=============================================================================
/// @file Competition Manager routines
/// @author guest_none (Patrick Rećko) <guest_none@protonmail.com>
//=============================================================================
//  Copyright (C) Patrick Rećko, all rights reserved.
//  This source file is released under MirOS License
//=============================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NiceIO;
using Newtonsoft.Json;

namespace MatchHelper.Core {

   /// <summary>
   /// This class is responsible for storing competitions and it's results
   /// and it's is also responsible for loading and saving state of the system
   /// </summary>
   public class CompetitionMgr {

      #region Private functions and structures

      public List<Competition> competition_storage = new List<Competition>();
      public List<CompetitionResults> results_storage = new List<CompetitionResults>();

      #endregion // Private functions and structures

      #region Public functions
      
      /// <summary>
      /// Load the system state (both stored competitions and results) 
      /// from the folder
      /// Note: files will be overwritten
      /// </summary>
      /// <param name="file_path">NiceIO object contating the path to the system state</param>
      public void loadSystemState(NPath file_path)
      {
         NPath read_compo = file_path.Combine("competition.json");
         NPath read_results = file_path.Combine("results.json");
         string json_read_compo = read_compo.ReadAllText();
         string json_read_results = read_results.ReadAllText();

         competition_storage = JsonConvert.DeserializeObject<List<Competition>>(json_read_compo);
         results_storage = JsonConvert.DeserializeObject<List<CompetitionResults>>(json_read_results);
      }

      /// <summary>
      /// Load the system state (both stored competitions and results) 
      /// from the folder
      /// </summary>
      /// <param name="path">NiceIO object contating the path to the system state</param>
      public void saveSystemState(NPath path)
      {
         NPath out_compo = path.CreateFile("competition.json");
         NPath out_results = path.CreateFile("results.json");
         string json_out_compo = JsonConvert.SerializeObject(competition_storage,
                                                             Formatting.Indented);
         string json_out_results = JsonConvert.SerializeObject(results_storage,
                                                               Formatting.Indented);
         out_compo.WriteAllText(json_out_compo);
         out_results.WriteAllText(json_out_results);

      }

      public List<Competition> getCompetitionList()
      {
         return this.competition_storage;
      }

      public List<CompetitionResults> getResultsList()
      {
         return this.results_storage;
      }

      public void addCompetition(Competition compo)
      {
         competition_storage.Add(compo);
      }

      public void addResults(CompetitionResults results)
      {
         results_storage.Add(results);
      }
      #endregion // Public functions 
   }
}
