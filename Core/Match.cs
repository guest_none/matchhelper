﻿//=============================================================================
/// @file Match routines
/// @author guest_none (Patrick Rećko) <guest_none@protonmail.com>
//=============================================================================
//  Copyright (C) Patrick Rećko, all rights reserved.
//  This source file is released under MirOS License
//=============================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatchHelper.Core {
   public enum WinnerState {
      first_team,
      second_team,
      draw,
      not_played
   };

   /// <summary>
   /// This class is responsible for storing informations about
   /// the matches
   /// </summary>
   public class Match {
      
      #region Private functions and structures

      public Team first_team;
      public Team second_team;
      public int first_team_points = 0;
      public int second_team_points = 0;
      public Referee referee;
      public bool was_played = false;
      public WinnerState winner = WinnerState.not_played;
      public DateTime play_date;
      protected void setWinnerAndPlayState()
      {
         if (first_team_points == second_team_points)
            this.winner = WinnerState.draw;
         else if (first_team_points > second_team_points)
            this.winner = WinnerState.first_team;
         else
            this.winner = WinnerState.second_team;

         this.was_played = true;
      }

      #endregion // Private functions and structures

      #region Public functions

      public Match()
      {

      }

      /// <summary>
      /// Creates a new Match object
      /// </summary>
      /// <param name="first">First team that will be playing</param>
      /// <param name="second">Second team that will be playing</param>
      /// <param name="main_referee">Main referee</param>
      /// <param name="date">Play date</param>
      public Match(Team first,
                   Team second,
                   Referee main_referee,
                   DateTime date)
      {
         this.first_team = first;
         this.second_team = second;
         this.referee = main_referee;
         this.play_date = date;
      }

      public Team getFirstTeamInfo()
      {
         return this.first_team;
      }

      public Team getSecondTeamInfo()
      {
         return this.second_team;
      }

      public Referee getRefereeInfo()
      {
         return this.referee;
      }

      public WinnerState getWinner()
      {
         if (this.was_played == false)
            return this.winner;
         else
            return this.winner;
      }

      /// <summary>
      /// Sets points after play and chooses winner based on highiest 
      /// number of points
      ///
      /// Note: this method is meant to be implemented by each type of
      ///       the class interited from the Match class 
      /// </summary>
      /// <param name="raw_first_points">Points get by first team</param>
      /// <param name="raw_second_points">Points get by second team</param>
      /// <param name="penalty_first_points">Penalty points get by first team</param>
      /// <param name="penalty_second_points">Penalty points get by second team team</param>
      public void setResults(int raw_first_points,
                                     int raw_second_points,
                                     int penalty_first_points,
                                     int penalty_second_points)
      {
         this.first_team_points = raw_first_points - penalty_first_points;
         this.second_team_points = raw_second_points - penalty_second_points;
         this.setWinnerAndPlayState();
      }

      public int getFirstTeamPoints()
      {
         return this.first_team_points;
      }

      public int getSecondTeamPoints()
      {
         return this.second_team_points;
      }

      public void changeDate(DateTime date)
      {
         this.play_date = date;
      }

      #endregion // Public functions 
   }

   #region Competition type specific classes

   /// <summary>
   /// Same as Match class
   /// </summary>
   public class TwoFires : Match {

      public TwoFires(Team first,
                      Team second,
                      Referee main_referee,
                      DateTime date)
      {
         base.first_team = first;
         base.second_team = second;
         base.referee = main_referee;
         base.play_date = date;
      }

      /// <summary>
      /// This classes specific Result setting method
      /// please see Match.setResults() for more informations
      /// </summary>
      /// <param name="raw_first_points">Points gathered by first team</param>
      /// <param name="raw_second_points">Points gathered by second team</param>
      public new void setResults(int raw_first_points,
                                     int raw_second_points)
      {
         base.first_team_points = raw_first_points;
         base.second_team_points = raw_second_points;
         base.setWinnerAndPlayState();
      }


   }

   /// <summary>
   /// Same as Match class
   /// </summary>
   public class LinePull : Match {

      public LinePull(Team first,
                      Team second,
                      Referee main_referee,
                      DateTime date)
      {
         base.first_team = first;
         base.second_team = second;
         base.referee = main_referee;
         base.play_date = date;
      }

      /// <summary>
      /// This classes specific Result setting method
      /// please see Match.setResults() for more informations
      /// </summary>
      /// <param name="raw_first_points">Points gathered by first team</param>
      /// <param name="raw_second_points">Points gathered by second team</param>
      public new void setResults(int raw_first_points,
                                     int raw_second_points)
      {
         base.first_team_points = raw_first_points;
         base.second_team_points = raw_second_points;
         base.setWinnerAndPlayState();
      }

   }

   /// <summary>
   /// Same as Match, but it also stores informations about
   /// two additional referees
   /// </summary>
   public class Volleyball : Match {

      #region Private functions and structures

      private Referee first_helper_referee;
      private Referee second_helper_referee;

      /// <summary>
      /// Creates a new Vollleyball specific Match object
      /// </summary>
      /// <param name="first">First team that will be playing</param>
      /// <param name="second">Second team that will be playing</param>
      /// <param name="main_referee">Main referee</param>
      /// <param name="first_helper">First helper referee</param>
      /// <param name="second_helper">Second helper referee</param>
      /// <param name="date">Play date</param>
      public Volleyball(Team first,
                        Team second,
                        Referee main_referee,
                        Referee first_helper,
                        Referee second_helper,
                        DateTime date)
      {
         base.first_team = first;
         base.second_team = second;
         base.referee = main_referee;
         base.play_date = date;
         this.first_helper_referee = first_helper;
         this.second_helper_referee = second_helper;
      }

      #endregion // Private functions and structures

      #region Public functions

      public Referee getFirstHelperReferee()
      {
         return first_helper_referee;
      }

      public Referee getSecondHelperReferee()
      {
         return second_helper_referee;
      }

      /// <summary>
      /// This classes specific Result setting method
      /// please see Match.setResults() for more informations
      /// </summary>
      /// <param name="raw_first_points">Points gathered by first team</param>
      /// <param name="raw_second_points">Points gathered by second team</param>
      /// <param name="penalty_first_points">Penalty points get by first team</param>
      /// <param name="penalty_second_points">Penalty points get by second team team</param>
      public new void setResults(int raw_first_points,
                                      int raw_second_points,
                                      int penalty_first_points,
                                      int penalty_second_points)
      {
         this.first_team_points = raw_first_points - penalty_first_points;
         this.second_team_points = raw_second_points - penalty_second_points;
         this.setWinnerAndPlayState();
      }
      #endregion // Public functions 
   }

   #endregion //Competition type specific classes

   class GetWinnerOnNonPlayedExcept : Exception {
      public GetWinnerOnNonPlayedExcept()
      {
      }

      public GetWinnerOnNonPlayedExcept(string msg) : base(msg)
      {
      }
   }

}
