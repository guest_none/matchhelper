﻿//=============================================================================
/// @file Competition routines
/// @author guest_none (Patrick Rećko) <guest_none@protonmail.com>
//=============================================================================
//  Copyright (C) Patrick Rećko, all rights reserved.
//  This source file is released under MirOS License
//=============================================================================

using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatchHelper.Core {

   public enum StageType {
      eliminations,
      quarterfinal,
      halffinals,
      finals
   };

   public enum CompetitionType {
      volleyball,
      twofires,
      linepull
   };

   /// <summary>
   /// This class is responisible for storing informations about the 
   /// competition and it's matches. It also does results geneeration
   /// </summary>
   public class Competition {
      public CompetitionType compo_type;
      public StageType stage_type;
      public List<Match> match_storage = new List<Match>();
      public List<Team> team_storage = new List<Team>();
      public List<Referee> referee_storage = new List<Referee>();

      private List<Team> generateNewTeamsStorage(int number_of_teams)
      {
         SortedList<int, Team> sorted_team_tmp = generatePointTable();
         List<Team> new_teams = new List<Team>();
         if (number_of_teams > sorted_team_tmp.Count()) {
            for (int i = 0; i < sorted_team_tmp.Count(); i++) {
               new_teams.Add(sorted_team_tmp.Values[i]);
            }
         } else {
            for (int i = 0; i < number_of_teams; i++) {
               new_teams.Add(sorted_team_tmp.Values[sorted_team_tmp.Count() - i - 1]);
            }
         }

         return new_teams;
      } 

      private SortedList<int, Team> generatePointTable()
      {
         SortedList<int, Team> sorted_team_tmp = new SortedList<int, Team>(new DuplicateKeyComparer<int>());
         for (int i = 0; i<this.team_storage.Count(); i++) {
            sorted_team_tmp.Add(gatherAllPoints(team_storage[i]), team_storage[i]);
         }
         return sorted_team_tmp;
      }

      private int gatherAllPoints(Team team)
      {
         int points = 0;
         foreach (var match in match_storage) {
            Team t1 = match.getFirstTeamInfo();
            Team t2 = match.getSecondTeamInfo();
            if (team.getTeamName() == t1.getTeamName())
               points += match.getFirstTeamPoints();
            else if (team.getTeamName() == t2.getTeamName())
               points += match.getSecondTeamPoints();
         }
         return points;
      }

      private Place makePlayerPlace(Team team, int place)
      {
         int points = gatherAllPoints(team);
         return new Place(team, place, points);
      }

      private Referee pickRefereeByType(CompetitionType type)
      {
         for (;;) {
            Referee r_tmp = this.referee_storage.PickRandom();
            if (r_tmp.getSpeciality().Contains(type) == true)
               return r_tmp;
         }
      }

      private Referee forVolleyballPickFirstHelper(CompetitionType type,
                                                   Referee main)
      {
         for (;;) {
            Referee r_tmp = this.referee_storage.PickRandom();
            if (r_tmp.getSpeciality().Contains(type) == true)
               if (r_tmp.getFirstName() != main.getFirstName() ||
                   r_tmp.getLastName() != main.getLastName())
                  return r_tmp;
         }
      }

      private Referee forVolleyballPickSecondHelper(CompetitionType type,
                                                    Referee main,
                                                    Referee first_hlp)
      {
         // linus torvalds probably gonna kill me
         for (;;) {
            Referee r_tmp = this.referee_storage.PickRandom();
            if (r_tmp.getSpeciality().Contains(type) == true)
               if (r_tmp.getFirstName() != main.getFirstName() ||
                   r_tmp.getLastName() != main.getLastName())
                   if (r_tmp.getFirstName() != main.getFirstName() ||
                       r_tmp.getLastName() != main.getLastName())
                     return r_tmp;
         }
      }
      #region Public functions

      /// <summary>
      /// Creates new competition
      /// </summary>
      /// <param name="c_type"> Type of competition</param>
      /// <param name="s_type"> Type of stage</param>
      public Competition(CompetitionType c_type, StageType s_type)
      {
         this.compo_type = c_type;
         this.stage_type = s_type;
      }

      /// <summary>
      /// Prepares a list of matches to play
      /// </summary>
      public void prepareMatches()
      {
         for (var i = 0; i < team_storage.Count(); i++) {
            for (var j = i + 1; j < team_storage.Count(); j++) {
               if (this.compo_type == CompetitionType.volleyball) {
                  var jm = pickRefereeByType(this.compo_type);
                  var jh1 = forVolleyballPickFirstHelper(this.compo_type,
                                                         jm);
                  var jh2 = forVolleyballPickSecondHelper(this.compo_type,
                                                          jm, jh1);
                  match_storage.Add(new Volleyball(team_storage[i],
                                                   team_storage[j],
                                                   jm, jh1, jh2, new DateTime()));
               } else if (this.compo_type == CompetitionType.linepull) {
                  match_storage.Add(new LinePull(team_storage[i],
                                                 team_storage[j],
                                                 pickRefereeByType(CompetitionType.linepull),
                                                 new DateTime()));
               } else if (this.compo_type == CompetitionType.twofires){
                  match_storage.Add(new TwoFires(team_storage[i],
                                                 team_storage[j],
                                                 pickRefereeByType(CompetitionType.twofires),
                                                 new DateTime()));
               }

            }
         }
      }

      public StageType getStageType()
      {
         return this.stage_type;
      }

      public CompetitionType getCompoType()
      {
         return this.compo_type;
      }

      public void addTeam(Team team)
      {
         this.team_storage.Add(team);
      }

      public void addReferee(Referee referee)
      {
         this.referee_storage.Add(referee);
      }

      public void addMatch(Team first, Team second)
      {
         if (this.compo_type == CompetitionType.volleyball) {
            var jm = pickRefereeByType(this.compo_type);
            var jh1 = forVolleyballPickFirstHelper(this.compo_type,
                                                   jm);
            var jh2 = forVolleyballPickSecondHelper(this.compo_type,
                                                    jm, jh1);
            match_storage.Add(new Volleyball(first,
                                             second,
                                             jm, jh1, jh2, new DateTime()));
         } else if (this.compo_type == CompetitionType.linepull) {
            match_storage.Add(new LinePull(first,
                                           second,
                                           pickRefereeByType(CompetitionType.linepull),
                                           new DateTime()));
         } else if (this.compo_type == CompetitionType.twofires) {
            match_storage.Add(new TwoFires(first,
                                           second,
                                           pickRefereeByType(CompetitionType.twofires),
                                           new DateTime()));
         }
      }

      public List<Team> getTeamList()
      {
         return this.team_storage;
      }

      public List<Referee> getRefereesList()
      {
         return this.referee_storage;
      }


      public List<Match> getMatchList()
      {
         return this.match_storage;
      }

      /// <summary>
      /// Generates a new Competition results object based from data stored
      /// in this called object
      /// </summary>
      /// <returns>New CompetitionResults object</returns>
      public CompetitionResults generateCompoResults()
      {
         SortedList<int, Team> sorted_team_tmp = generatePointTable();
         SortedList<int, Place> place_list = new SortedList<int, Place>();
         int i = 0;
         for(int j = sorted_team_tmp.Count(); j>0; j--) {
            place_list.Add(i+1, new Place(sorted_team_tmp.Values[j-1], i+1, sorted_team_tmp.Keys[j-1]));
            i += 1;
         }
         return new CompetitionResults(this.stage_type, this.compo_type, place_list[1].getTeam(), place_list);
      }

      /// <summary>
      /// Sets the competition to next stage and cleans up teams base on this formula
      ///    - 8 or less teams remaining for quaterfinals
      ///    - 4 or less teams remaining for halffinals
      ///    - 2 for the finals
      /// </summary>
      /// <param name="type"> Type of stage for jump</param>
      public void prepareForNextStage(StageType type)
      {
         List<Team> t = null;
         if (type == StageType.quarterfinal) {
            t = generateNewTeamsStorage(8);
         } else if (type == StageType.halffinals) {
            t = generateNewTeamsStorage(4);
         } else if (type == StageType.finals) {
            t = generateNewTeamsStorage(2);
         }
         this.team_storage = t;
         this.match_storage = new List<Match>();
         this.stage_type = type;
      }

      #endregion // Public functions 
   }
}
