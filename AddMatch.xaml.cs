﻿//=============================================================================
/// @file Add match Window
/// @author guest_none (Patrick Rećko) <guest_none@protonmail.com>
//=============================================================================
//  Copyright (C) Patrick Rećko, all rights reserved.
//  This source file is released under MirOS License
//=============================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MatchHelper.Core;
using MatchHelper;

namespace MatchHelper {
   /// <summary>
   /// Interaction logic for AddMatch.xaml
   /// </summary>
   public partial class AddMatch : Window {

      class TeamNameString {
         public string value
         {
            get; set;
         }
      }

      private List<string> _t = new List<string>();
      private Competition _c;
      private List<Team> _tadd;

      public AddMatch()
      {
         InitializeComponent();
      }

      public AddMatch(List<Team> t, Competition c)
      {
         InitializeComponent();
         foreach (var team in t) {
            _t.Add(team.getTeamName());
         }
         firstteamcombobox.ItemsSource = _t;
         secondteamcombobox.ItemsSource = _t;
         this._c = c;
         this._tadd = t;
      }

      public void addbutton_Click(object sender, RoutedEventArgs e)
      {
         this._c.addMatch(_tadd[firstteamcombobox.SelectedIndex], _tadd[secondteamcombobox.SelectedIndex]);
         this.Close();
      }

      public void cancelbutton_Click(object sender, RoutedEventArgs e)
      {
         this.Close();
      }

   }
}
