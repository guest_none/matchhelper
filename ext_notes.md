Software used in MatchHelper software
=====================================

This file lists code and libraries used in this software

Newtonsoft.Json
---------------
**Info:** An widly popular and tested library for reading, writing and manipulating JSON data

**Purpose:** Read/write system state in CompetitionMgr by the use of JsonObject.Serialize/Deserialize

**License:** MIT License

**Source:** https://github.com/JamesNK/Newtonsoft.Json

Copyright (c) 2007 James Newton-King

NiceIO
------
**Info:** Library for reading, writing and manipulating external files and directories. Essentially a saner version of System.IO.

**Purpose:** Read/write system state from folder in CompetitionMgr, general file manipulation.

**License:** MIT License

**Source:** https://github.com/lucasmeijer/NiceIO

Copyright © 2015-2017 Lucas Meijer

Nunit 3
-------
**Info:** Library for writing and executing tests

**Purpose:** Tests of Core module.

**License:** MIT License

**Source:** https://github.com/nunit/nunit

Copyright (c) 2017 Charlie Poole

net-object-deep-copy
--------------------
**Info:** Single-file Library for deep object copy.

**Purpose:** deep object copy for tests.

**License:** MIT License

**Source:** https://github.com/Burtsev-Alexey/net-object-deep-copy

Copyright (c) 2014 Burtsev Alexey


EnumerableExtension
-------------------
**Info:** Object extentions for random picks 

**Purpose:** Picking object from list randomly via PickRandom method (mainly for referree test)

**License:** MIT License

**Source:** https://stackoverflow.com/questions/2019417/access-random-item-in-list

Copyright (c) Mark Seemann

Ookii.Dialogs
-------------------
**Info:** An set of Windows Presentation Foundation dialogs

**Purpose:** Folder select in UI

**License:** BSD 3-clause

**Source:** http://www.ookii.org/Download/Dialogs

Copyright (c) Sven Groot

License Texts
=============

MIT License
-----------

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

BSD 3-clause
------------

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1) Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 

2) Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution. 

3) Neither the name of the ORGANIZATION nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.