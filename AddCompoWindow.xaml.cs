﻿//=============================================================================
/// @file Add competiiton window - Implementation
/// @author guest_none (Patrick Rećko) <guest_none@protonmail.com>
//=============================================================================
//  Copyright (C) Patrick Rećko, all rights reserved.
//  This source file is released under MirOS License
//=============================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MatchHelper.Core;

namespace MatchHelper {

   public class CompoTypeString {
      public string value
      {
         get; set;
      }
   }



   /// <summary>
   /// Interaction logic for AddCompoWindow.xaml
   /// </summary>
   public partial class AddCompoWindow : Window {

      private List<string> compolist
      {
         get; set;
      } = new List<string> { "Volleyball", "Two fires", "Line pull" };

      private List<string> stagelist
      {
         get; set;
      } = new List<string> { "Eliminations", "Quarterfinals", "Halffinals", "Finals" };

      private CompetitionType compoTypeToString(string s)
      {
         if (s == "Volleyball") {
            return CompetitionType.volleyball;
         } else if (s == "Two fires") {
            return CompetitionType.twofires;
         } else {
            return CompetitionType.linepull;
         }
      }

      private StageType stageTypeToString(string s)
      {
         if (s == "Eliminations") {
            return StageType.eliminations;
         } else if (s == "Quarterfinals") {
            return StageType.quarterfinal;
         } else if (s == "Halffinals") {
            return StageType.quarterfinal;
         } else {
            return StageType.finals;
         }
      }

      public AddCompoWindow()
      {
         InitializeComponent();
         this.compocombobox.ItemsSource = this.compolist;
         this.stagecombobox.ItemsSource = this.stagelist;
      }

      private void cancelbutton_Click(object sender, RoutedEventArgs e)
      {
         this.Close();
      }

      private void addbutton_Click(object sender, RoutedEventArgs e)
      {
         if (compocombobox.SelectedItem == null || stagecombobox.SelectedItem == null) {
            MessageBox.Show("Please Select your choice", "error", MessageBoxButton.OK);
         } else {
            App.g_mgr.addCompetition(new Competition(compoTypeToString(compocombobox.SelectedItem.ToString()), stageTypeToString(stagecombobox.SelectedItem.ToString())));
            this.Close();
         }
      }
   }
}
