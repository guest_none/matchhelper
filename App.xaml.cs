﻿//=============================================================================
/// @file Application entry point and globals
/// @author guest_none (Patrick Rećko) <guest_none@protonmail.com>
//=============================================================================
//  Copyright (C) Patrick Rećko, all rights reserved.
//  This source file is released under MirOS License
//=============================================================================

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using MatchHelper.Core;

namespace MatchHelper {

   /// <summary>
   /// Interaction logic for App.xaml
   /// </summary>
   public partial class App : Application
   {
      public static CompetitionMgr g_mgr { get; set; } = new CompetitionMgr();
      public static bool is_changed  { get; set; } = false;
      public static bool is_unsaved { get; set; } = false;
   }

}
