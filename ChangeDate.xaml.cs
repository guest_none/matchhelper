﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MatchHelper {
   /// <summary>
   /// Interaction logic for ChangeDate.xaml
   /// </summary>
   public partial class ChangeDate : Window {

      public DateTime _date
      {
         get;
         set;
      }

      public ChangeDate()
      {
         InitializeComponent();
      }

      private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
      {
         var picker = sender as DatePicker;
         DateTime? date = picker.SelectedDate;
         if (date == null) {
            // ... A null object.
            _date = new DateTime(2010, 1, 1);
         } else {
            // ... No need to display the time.
            _date = (DateTime)date;
         }
      }

      private void button_Click(object sender, RoutedEventArgs e)
      {
         this.Close();
      }
   }
}
