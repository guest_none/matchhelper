MatchHelper
===========

This application was written as an university project and is a app for managing competitions organized on beaches and it's results. It is written in C# using Windows Presentation Foundation framework

The sources are avaiable on: https://bitbucket.org/guest_none/matchhelper

Build
-----
Microsoft Visual Studio 2015 is required. NuGet packages support must be turned on. To compile load MatchHelper.sln solution and compile. To run either use built-in debugger or run from the bin/"Configuration" folder.

To run tests either use TestDriven.Net or after first compilation run script "testdebug.bat" or "testrelease.bat" depending on build configuration.


Work division
-------------
Patirick Rećko - Most of the code.

Sławomir Kraśko - documentation and window design

Notes on code
-------------

This code is a mixture of quite nice code and total garbage written in few days.

Generaly the code is seperated in 3 locations:

   - Root folder contains sources for UI
   - Core folder contains sources for core structs (MatchHelper.Core module)
   - Tests folder contains sources for tests.
   

Note on Problems
----------------
Due of the lack of time there might be some problems:

   - [UI] Numerical input for Points and team size takes white spaces while it shouldn't
   - [UI] UI doesn't recognize the distinction between Match types
   - [UI] Match type specific Match editing and result setting windows are not finished
   - [Core] Referees in Volleyball type might be not saved - this should be investigated
   
Those bugs will be fixed before presentation.   

Notes on documentation
----------------------
Most of the code documentation was written for Core module, generaly most of the code for Core is self documenting.

License
-------
Licensed under MirOS license as described in license.md file in sources