﻿//=============================================================================
/// @file Competition test
/// @author guest_none (Patrick Rećko) <guest_none@protonmail.com>
//=============================================================================
//  Copyright (C) Patrick Rećko, all rights reserved.
//  This source file is released under MirOS License
//=============================================================================

using NUnit.Framework;
using MatchHelper.Core;
using System.Collections.Generic;

namespace MatchHelper.Tests {

   [TestFixture]
   public class CompetitionTest {
      Competition c_lp = new Competition(CompetitionType.linepull, StageType.halffinals);

      private Team createControlledLinePullTeam(string name)
      {
         Team t = new Team(name, 4);
         t.addMember(new TeamMember("aaa", "bbb", TeamMemberType.captain));
         t.addMember(new TeamMember("ccc", "ddd", TeamMemberType.normal));
         t.addMember(new TeamMember("eee", "fff", TeamMemberType.normal));
         t.addMember(new TeamMember("ggg", "hhh", TeamMemberType.normal));
         return t;
      }

      private void prepareLinePullCompo()
      {
         c_lp.addTeam(createControlledLinePullTeam("First"));
         c_lp.addTeam(createControlledLinePullTeam("Second"));
         c_lp.addTeam(createControlledLinePullTeam("Third"));
         c_lp.addReferee(new Referee("aaa", "bbb", new List<CompetitionType>{ CompetitionType.linepull }));
         c_lp.addReferee(new Referee("ccc", "ddd", new List<CompetitionType> { CompetitionType.linepull, CompetitionType.volleyball }));
         c_lp.prepareMatches();
      }



      [Test]
      public void linePullCompoResults()
      {
         prepareLinePullCompo();
         var m_list = this.c_lp.getMatchList();
         Assert.AreEqual("Third", m_list[2].getSecondTeamInfo().getTeamName());
         var m_tmp = (LinePull)this.c_lp.match_storage[0];
         m_tmp.setResults(1, 2);
         this.c_lp.match_storage[0] = (Match)m_tmp;
         m_tmp = (LinePull)this.c_lp.match_storage[1];
         m_tmp.setResults(1, 1);
         this.c_lp.match_storage[1] = (Match)m_tmp;
         m_tmp = (LinePull)this.c_lp.match_storage[2];
         m_tmp.setResults(3, 1);
         this.c_lp.match_storage[2] = (Match)m_tmp;
         var r = c_lp.generateCompoResults();
         Assert.AreEqual("Second", r.getWinner().getTeam().getTeamName());
         Assert.AreEqual(5, r.getWinner().getPoints());
         this.c_lp.prepareForNextStage(StageType.finals);
         Assert.AreEqual("Second", this.c_lp.team_storage[0].getTeamName());
      }
   }

}