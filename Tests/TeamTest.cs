﻿//=============================================================================
/// @file Team test
/// @author guest_none (Patrick Rećko) <guest_none@protonmail.com>
//=============================================================================
//  Copyright (C) Patrick Rećko, all rights reserved.
//  This source file is released under MirOS License
//=============================================================================

using NUnit.Framework;
using MatchHelper.Core;
using System;
using System.Collections.Generic;

namespace MatchHelper.Tests {

   [TestFixture]
   public class TeamTest {
      private Team t = new Team("Examples", 4);

      private void prepare()
      {
         t.addMember(new TeamMember("aaa", "bbb", TeamMemberType.normal));
         t.addMember(new TeamMember("ccc", "ddd", TeamMemberType.normal));
         t.addMember(new TeamMember("eee", "fff", TeamMemberType.normal));
      }

      [Test]
      public void testWithoutTheCaptain()
      {
         prepare();
         var t_woutc = ObjectExtensions.Copy(this.t);
         t_woutc.addMember(new TeamMember("ggg", "hhh", TeamMemberType.normal));
         List<TeamMember> l = t_woutc.getMemberList();

         Assert.AreEqual("aaa", l[0].getFirstName());
         Assert.AreEqual("bbb", l[0].getLastName());
         Assert.AreEqual(TeamMemberType.normal, l[0].getMemberType());

         Assert.AreEqual("ccc", l[1].getFirstName());
         Assert.AreEqual("ddd", l[1].getLastName());
         Assert.AreEqual(TeamMemberType.normal, l[1].getMemberType());

         Assert.AreEqual("eee", l[2].getFirstName());
         Assert.AreEqual("fff", l[2].getLastName());
         Assert.AreEqual(TeamMemberType.normal, l[2].getMemberType());

         Assert.AreEqual("ggg", l[3].getFirstName());
         Assert.AreEqual("hhh", l[3].getLastName());
         Assert.AreEqual(TeamMemberType.normal, l[3].getMemberType());

         Assert.AreEqual(false, t_woutc.isCaptainAdded());
      }

      public void testWithTheCaptain()
      {
         prepare();
         var t_woutc = ObjectExtensions.Copy(this.t);
         t_woutc.addMember(new TeamMember("ggg", "hhh", TeamMemberType.captain));
         List<TeamMember> l = t_woutc.getMemberList();

         Assert.AreEqual("aaa", l[0].getFirstName());
         Assert.AreEqual("bbb", l[0].getLastName());
         Assert.AreEqual(TeamMemberType.normal, l[0].getMemberType());

         Assert.AreEqual("ccc", l[1].getFirstName());
         Assert.AreEqual("ddd", l[1].getLastName());
         Assert.AreEqual(TeamMemberType.normal, l[1].getMemberType());

         Assert.AreEqual("eee", l[2].getFirstName());
         Assert.AreEqual("fff", l[2].getLastName());
         Assert.AreEqual(TeamMemberType.normal, l[2].getMemberType());

         Assert.AreEqual("ggg", l[3].getFirstName());
         Assert.AreEqual("hhh", l[3].getLastName());
         Assert.AreEqual(TeamMemberType.normal, l[3].getMemberType());

         Assert.AreEqual(true, t_woutc.isCaptainAdded());
      }
   }

}