﻿//=============================================================================
/// @file Competition Manager test
/// @author guest_none (Patrick Rećko) <guest_none@protonmail.com>
//=============================================================================
//  Copyright (C) Patrick Rećko, all rights reserved.
//  This source file is released under MirOS License
//=============================================================================

using NUnit.Framework;
using System.Collections.Generic;
using System;
using MatchHelper.Core;
using NiceIO;

namespace MatchHelper.Tests {

   [TestFixture]
   public class CompetitionMgrTest {
      private CompetitionMgr mgr = new CompetitionMgr();
      private CompetitionMgr mgr_read = new CompetitionMgr();

      public void createFakeResults()
      {
         Team w = new Team("Winners", 2);
         SortedList<int, Place> c = new SortedList<int, Place>();
         Place pl = new Place(w, 1, 10);
         c.Add(pl.getPlace(), pl.Copy());
         pl = new Place(new Team("Seconders", 2), 2, 6);
         c.Add(pl.getPlace(), pl.Copy());
         pl = new Place(new Team("Thirdthies", 2), 3, 3);
         c.Add(pl.getPlace(), pl.Copy());
         this.mgr.addResults(new CompetitionResults(StageType.halffinals, CompetitionType.linepull, w, c.Copy()));
      }

      private void createCompetition()
      {
         this.mgr.addCompetition(new Competition(CompetitionType.linepull, StageType.halffinals));
         this.mgr.getCompetitionList()[0].addTeam(new Team("Winners", 2));
         this.mgr.getCompetitionList()[0].addTeam(new Team("Seconders", 2));
         this.mgr.getCompetitionList()[0].addTeam(new Team("Thirdies", 2));
         this.mgr.getCompetitionList()[0].getTeamList()[0].addMember(new TeamMember("aaa", "bbb", TeamMemberType.captain));
         this.mgr.getCompetitionList()[0].getTeamList()[0].addMember(new TeamMember("ccc", "ddd", TeamMemberType.normal));
         this.mgr.getCompetitionList()[0].getTeamList()[1].addMember(new TeamMember("aaa", "bbb", TeamMemberType.captain));
         this.mgr.getCompetitionList()[0].getTeamList()[1].addMember(new TeamMember("ccc", "ddd", TeamMemberType.normal));
         this.mgr.getCompetitionList()[0].getTeamList()[2].addMember(new TeamMember("aaa", "bbb", TeamMemberType.captain));
         this.mgr.getCompetitionList()[0].getTeamList()[2].addMember(new TeamMember("ccc", "ddd", TeamMemberType.normal));
         this.mgr.getCompetitionList()[0].addReferee(new Referee("aaa", "bbbr1", new List<CompetitionType> { CompetitionType.volleyball, CompetitionType.linepull }));
         this.mgr.getCompetitionList()[0].addReferee(new Referee("aaa", "bbbr2", new List<CompetitionType> { CompetitionType.volleyball, CompetitionType.linepull }));

      }

      private void createRealResults()
      {
         mgr.addResults(mgr.getCompetitionList()[0].generateCompoResults());
      }

      private void prepare()
      {

         // add fake results
         createFakeResults();
      }

      private void checkReadData()
      {
         Assert.AreEqual("Winners", mgr_read.getResultsList()[0].getWinner().getTeam().getTeamName());
      }

      [Test]
      public void saveAndLoadTest()
      {
         prepare();
         createCompetition();
         NPath temp = NPath.CreateTempDirectory("test");
         this.mgr.saveSystemState(temp);
         this.mgr_read.loadSystemState(temp);
         checkReadData();
      }

   }

}