﻿//=============================================================================
/// @file Referee test
/// @author guest_none (Patrick Rećko) <guest_none@protonmail.com>
//=============================================================================
//  Copyright (C) Patrick Rećko, all rights reserved.
//  This source file is released under MirOS License
//=============================================================================

using NUnit.Framework;
using System;
using System.Collections.Generic;
using MatchHelper.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatchHelper.Tests {
   [TestFixture]
   public class RefereeTest {

      [Test]
      public void createRefereeWithOneSpecialization()
      {
         string first = "aaa";
         string last = "bbb";
         List<CompetitionType> spec = new List<CompetitionType> { CompetitionType.linepull };
         Referee r = new Referee(first, last, spec);
         Assert.AreEqual("aaa", r.getFirstName());
         Assert.AreEqual("bbb", r.getLastName());
         List<CompetitionType> t = r.getSpeciality(); 
         Assert.AreEqual(spec[0], t[0]);
      }

      [Test]
      public void createRefereeWithMoreSpecialization()
      {
         string first = "aaa";
         string last = "bbb";
         List<CompetitionType> spec = new List<CompetitionType> { CompetitionType.linepull , CompetitionType.twofires};
         Referee r = new Referee(first, last, spec);
         Assert.AreEqual("aaa", r.getFirstName());
         Assert.AreEqual("bbb", r.getLastName());
         List<CompetitionType> t = r.getSpeciality();
         Assert.AreEqual(spec[0], t[0]);
         Assert.AreEqual(spec[1], t[1]);
      }
   }
}
