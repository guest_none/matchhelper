﻿//=============================================================================
/// @file Match test
/// @author guest_none (Patrick Rećko) <guest_none@protonmail.com>
//=============================================================================
//  Copyright (C) Patrick Rećko, all rights reserved.
//  This source file is released under MirOS License
//=============================================================================

using NUnit.Framework;
using MatchHelper.Core;
using System.Collections.Generic;
using System;

namespace MatchHelper.Tests {

   [TestFixture]
   public class MatchTest {

      private Match m;
      private Team t1;
      private Team t2;
      private Referee r = new Referee("Jacob",
                                      "Jacobson",
                                      new List<CompetitionType> { CompetitionType.linepull,
                                                                  CompetitionType.volleyball});
      private Referee r_h1 = new Referee("Andrew",
                                         "Andrewson",
                                         new List<CompetitionType> { CompetitionType.volleyball });
      private Referee r_h2 = new Referee("Jan",
                                         "Kowalsky",
                                         new List<CompetitionType> { CompetitionType.volleyball });

      private void prepareTeams()
      {
         t1.addMember(new TeamMember("aaa", "bbb", TeamMemberType.captain));
         t1.addMember(new TeamMember("ccc", "ddd", TeamMemberType.normal));
         t1.addMember(new TeamMember("ccc", "ddd", TeamMemberType.normal));
         t2.addMember(new TeamMember("aaa", "bbb", TeamMemberType.captain));
         t2.addMember(new TeamMember("ccc", "ddd", TeamMemberType.normal));
         t2.addMember(new TeamMember("ccc", "ddd", TeamMemberType.normal));
      }

      private void prepare()
      {
         this.t1 = new Team("Lols", 3);
         this.t2 = new Team("Lmaos", 3);
         prepareTeams();
      }

      [Test]
      public void noTypeMatchTest()
      {
         prepare();
         m = new Match(t1, t2, r, DateTime.Now);
         m.setResults(5, 2, 2, 0);
         Assert.AreEqual(WinnerState.first_team, m.getWinner());
         Assert.AreEqual(3, m.getFirstTeamPoints());
         Assert.AreEqual(2, m.getSecondTeamPoints());
         // standard data input check
         Assert.AreEqual("aaa", m.getFirstTeamInfo().getCaptain().getFirstName());
      }

      [Test]
      public void volleyballMatchTest()
      {
         prepare();
         m = new Volleyball(t1, t2, r, r_h1, r_h2, DateTime.Now);

         if (m is Volleyball) {
            Volleyball m_vol = (Volleyball)m;
            Assert.AreEqual("Andrew", m_vol.getFirstHelperReferee().getFirstName());
            m_vol.setResults(2, 5, 0, 2);
            Assert.AreEqual(WinnerState.second_team, m_vol.getWinner());
            Assert.AreEqual(2, m_vol.getFirstTeamPoints());
            Assert.AreEqual(3, m_vol.getSecondTeamPoints());
         } else {
            Assert.Fail("Typecast Error");
         }
      }

      [Test]
      public void linePullTest()
      {
         prepare();
         m = new LinePull(t1, t2, r, DateTime.Now);

         if (m is LinePull) {
            var m_tmp = (LinePull)m;
            m_tmp.setResults(2, 2);
            Assert.AreEqual(WinnerState.draw, m_tmp.getWinner());
         } else {
            Assert.Fail("Typecast Error");
         }
      }
   }

}