﻿//=============================================================================
/// @file Competition results test
/// @author guest_none (Patrick Rećko) <guest_none@protonmail.com>
//=============================================================================
//  Copyright (C) Patrick Rećko, all rights reserved.
//  This source file is released under MirOS License
//=============================================================================

using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MatchHelper.Core;

namespace MatchHelper.Tests {

   [TestFixture]
   public class CompetitionResultsTest {
      private CompetitionResults cres;

     
      public void createResults()
      {
         Team w = new Team("Winners", 2);
         SortedList<int, Place> c = new SortedList<int, Place>();
         Place pl = new Place(w, 1, 10);
         c.Add(pl.getPlace(), pl.Copy());
         pl = new Place(new Team("Seconders", 2), 2, 6);
         c.Add(pl.getPlace(), pl.Copy());
         pl = new Place(new Team("Thirdthies", 2), 3, 3);
         c.Add(pl.getPlace(), pl.Copy());
         this.cres = new CompetitionResults(StageType.halffinals, CompetitionType.linepull, w, c.Copy());
      }

      [Test]
      public void resultsTest()
      {
         createResults();
         Assert.AreEqual("Winners", cres.getWinner().getTeam().getTeamName());
         Assert.AreEqual("Winners", cres.getClassification()[1].getTeam().getTeamName());
         Assert.AreEqual("Seconders", cres.getClassification()[2].getTeam().getTeamName());
         Assert.AreEqual("Thirdthies", cres.getClassification()[3].getTeam().getTeamName());
      }

      [Test]
      public void placeCreationTest() {

         Place p = new Place(new Team("a", 1), 1, 10);
         Assert.AreEqual(1, p.getPlace());
         Assert.AreEqual(10, p.getPoints());
         Assert.AreEqual("a", p.getTeam().getTeamName());
         Assert.AreEqual(1, p.getTeam().getTeamSize());
      }
   }
}
