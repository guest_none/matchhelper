﻿//=============================================================================
/// @file Team member test
/// @author guest_none (Patrick Rećko) <guest_none@protonmail.com>
//=============================================================================
//  Copyright (C) Patrick Rećko, all rights reserved.
//  This source file is released under MirOS License
//=============================================================================

using System;
using NUnit.Framework;
using System.Collections.Generic;
using MatchHelper.Core;

namespace MatchHelper.Tests {

   [TestFixture]
   public class TeamMemberTest {

      [Test]
      public void createNormalTeamMember()
      {
         TeamMember m = new TeamMember("aaa", "bbb", TeamMemberType.normal);
         Assert.AreEqual("aaa", m.getFirstName());
         Assert.AreEqual("bbb", m.getLastName());
         Assert.AreEqual(TeamMemberType.normal, m.getMemberType());
      }

      [Test]
      public void createCaptainTeamMember()
      {
         TeamMember m = new TeamMember("aaa", "bbb", TeamMemberType.captain);
         Assert.AreEqual("aaa", m.getFirstName());
         Assert.AreEqual("bbb", m.getLastName());
         Assert.AreEqual(TeamMemberType.captain, m.getMemberType());
      }
   }

}
