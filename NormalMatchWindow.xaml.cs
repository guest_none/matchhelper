﻿//=============================================================================
/// @file Normal match window
/// @author guest_none (Patrick Rećko) <guest_none@protonmail.com>
//=============================================================================
//  Copyright (C) Patrick Rećko, all rights reserved.
//  This source file is released under MirOS License
//=============================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MatchHelper.Core;
using MatchHelper;

namespace MatchHelper {
   /// <summary>
   /// Interaction logic for NormalMatchWindow.xaml
   /// </summary>
   public partial class NormalMatchWindow : Window {

      private Match _m;

      public NormalMatchWindow()
      {
         InitializeComponent();
      }

      public NormalMatchWindow(Match m)
      {
         InitializeComponent();
         this._m = m;
         firstteamtextBlock.Text = _m.getFirstTeamInfo().getTeamName();
         secondteamtextBlock.Text = _m.getSecondTeamInfo().getTeamName();
         refereetextBlock.Text = _m.getRefereeInfo().first_name + " " + _m.getRefereeInfo().last_name;
         refresh();
      }

      private void refresh()
      {
         firstpointtextBlock.Text = Convert.ToString(_m.getFirstTeamPoints());
         secondpointtextBlock.Text = Convert.ToString(_m.getSecondTeamPoints());
         dateBlock.Text = Convert.ToString(_m.play_date.Year) + "."+ Convert.ToString(_m.play_date.Month) + "." + Convert.ToString(_m.play_date.Day);
         winnertextBlock.Text = UIUtils.winnerStateToString(_m.getWinner());
      }

      private void setresultsButton_Click(object sender, RoutedEventArgs e)
      {
         SetResults w = new SetResults(_m);
         w.ShowDialog();
         refresh();
      }

      private void setdateButton_Click(object sender, RoutedEventArgs e)
      {
         ChangeDate w = new ChangeDate();
         w.ShowDialog();
         _m.changeDate(w._date);
         dateBlock.Text = Convert.ToString(_m.play_date.Year) + "." + Convert.ToString(_m.play_date.Month) + "." + Convert.ToString(_m.play_date.Day);
      }
   }
}
