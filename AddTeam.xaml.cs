﻿//=============================================================================
/// @file Add team
/// @author guest_none (Patrick Rećko) <guest_none@protonmail.com>
//=============================================================================
//  Copyright (C) Patrick Rećko, all rights reserved.
//  This source file is released under MirOS License
//=============================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MatchHelper.Core;

namespace MatchHelper {
   /// <summary>
   /// Interaction logic for AddTeam.xaml
   /// </summary>
   public partial class AddTeam : Window {
      public Team t
      {
         get;
         private set;
      }

      public bool to_add = false;

      public AddTeam()
      {
         InitializeComponent();
      }

      private void onlyNumberAllowed(object sender, TextCompositionEventArgs e)
      {
         Regex regex = new Regex("^[^0-9\\s]+$"); //regex that matches disallowed text
         e.Handled = regex.IsMatch(e.Text);
      }

      private void addButton_Click(object sender, RoutedEventArgs e)
      {
         if (nametextBox.Text == "" || sizetextBox.Text == "") {
            MessageBox.Show("Please type your data", "Error", MessageBoxButton.OK);
         } else {
            this.t = new Team(nametextBox.Text, Convert.ToInt32(sizetextBox.Text));
            this.to_add = true;
            this.Close();
         }
      }

      private void cancelButton_Click(object sender, RoutedEventArgs e)
      {
         this.Close();
      }
   }
}
