﻿using MatchHelper.Core;
using System;
using System.Collections.Generic;

namespace MatchHelper {
   public class UIUtils {

      //public static string teamMemberTypeToString(TeamMemberType t)
      //{

      //}

      public static string competitionTypeToString(CompetitionType t)
      {
         if (t == CompetitionType.linepull)
            return "Line Pull";
         else if (t == CompetitionType.twofires)
            return "Two fires";
         else
            return "Volleyball";
      }

      public static string stageTypeToString(StageType t)
      {
         if (t == StageType.eliminations)
            return "Eliminations";
         else if (t == StageType.quarterfinal)
            return "Quarter Finals";
         else if (t == StageType.halffinals)
            return "Half finals";
         else 
            return "Finals";
      }

      public static string specToString(List<CompetitionType> l)
      {
         string tmp = "";
         foreach (var t in l) {
            tmp += UIUtils.competitionTypeToString(t);
            tmp += ", ";
         }
         return tmp;
      }

      public static string winnerStateToString(WinnerState s)
      {
         if (s == WinnerState.first_team) {
            return "First Team";
         } else if (s == WinnerState.second_team) {
            return "Second team";
         } else if (s == WinnerState.draw) {
            return "Draw";
         } else {
            return "Not played";
         }
      }

      public static string teamMemberTypeToString(TeamMemberType t)
      {
         if (t == TeamMemberType.captain) {
            return "Captain";
         } else {
            return "Normal";
         }
      }
   }

}