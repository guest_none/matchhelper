﻿//=============================================================================
/// @file About window - Implementation
/// @author guest_none (Patrick Rećko) <guest_none@protonmail.com>
//=============================================================================
//  Copyright (C) Patrick Rećko, all rights reserved.
//  This source file is released under MirOS License
//=============================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MatchHelper {
   /// <summary>
   /// Interaction logic for About.xaml
   /// </summary>
   public partial class About : Window {
      public About()
      {
         InitializeComponent();
      }

      private void button_Click(object sender, RoutedEventArgs e)
      {
         this.Close();
      }

      

   }
}
