﻿//=============================================================================
/// @file Add Referee window
/// @author guest_none (Patrick Rećko) <guest_none@protonmail.com>
//=============================================================================
//  Copyright (C) Patrick Rećko, all rights reserved.
//  This source file is released under MirOS License
//=============================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MatchHelper.Core;

namespace MatchHelper {
   /// <summary>
   /// Interaction logic for Window1.xaml
   /// </summary>
   public partial class AddReferee : Window {

      private Competition _c;
      private List<CompetitionType> _ctype = new List<CompetitionType>();

      public AddReferee()
      {
         InitializeComponent();
      }

      public AddReferee(Competition c)
      {
         InitializeComponent();
         this._c = c;
      }

      private void gatherCompoTypes()
      {
         if (isvolleyballcheckBox.IsChecked == true) {
            this._ctype.Add(CompetitionType.volleyball);
         }
         if (islinepullcheckBox.IsChecked == true) {
            this._ctype.Add(CompetitionType.linepull);
         }
         if (istwofirescheckBox.IsChecked == true) {
            this._ctype.Add(CompetitionType.twofires);
         }
         if (this._ctype.Count == 0) {
            this._ctype.Add(CompetitionType.volleyball);
            this._ctype.Add(CompetitionType.linepull);
            this._ctype.Add(CompetitionType.twofires);
         }
      }

      private void addButton_Click(object sender, RoutedEventArgs e)
      {
         gatherCompoTypes();
         _c.addReferee(new Referee(firsttextBox.Text, lasttextBox.Text, _ctype.Copy()));
         this.Close();
      }

      private void cancelButton_Click(object sender, RoutedEventArgs e)
      {
         this.Close();
      }
   }
}
