﻿//=============================================================================
/// @file Results
/// @author guest_none (Patrick Rećko) <guest_none@protonmail.com>
//=============================================================================
//  Copyright (C) Patrick Rećko, all rights reserved.
//  This source file is released under MirOS License
//=============================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MatchHelper.Core;

namespace MatchHelper {
   /// <summary>
   /// Interaction logic for ResultsWindow.xaml
   /// </summary>
   public partial class ResultsWindow : Window {

      private class _Classification {
         public int place
         {
            get;
            set;
         }

         public string teamname
         {
            get;
            set;
         }

         public int pointnum
         {
            get;
            set;
         }

         public _Classification(int place, string teamname, int pointnum)
         {
            this.place = place;
            this.teamname = teamname;
            this.pointnum = pointnum;
         }
      }

      private CompetitionResults _r;
      private List<_Classification>  _c= new List<_Classification>();

      public ResultsWindow()
      {
         InitializeComponent();
         classificationView.ItemsSource = _c;
      }

      public ResultsWindow(CompetitionResults r)
      {
         InitializeComponent();
         this._r = r;
         this.populateList();
         this.compotextBlock.Text = UIUtils.competitionTypeToString(_r.getCompoType());
         this.stagetextBlock.Text = UIUtils.stageTypeToString(_r.getStageType());
         classificationView.ItemsSource = _c;
      }


      private void populateList()
      {
         foreach (var p in _r.classification) {
            this._c.Add(new _Classification(p.Value.getPlace(), p.Value.getTeam().getTeamName(), p.Value.getPoints()));
         }
      }

   }
}
