﻿//=============================================================================
/// @file Competition window
/// @author guest_none (Patrick Rećko) <guest_none@protonmail.com>
//=============================================================================
//  Copyright (C) Patrick Rećko, all rights reserved.
//  This source file is released under MirOS License
//=============================================================================


using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MatchHelper.Core;
using MatchHelper;

namespace MatchHelper {
   /// <summary>
   /// Interaction logic for CompoWindow.xaml
   /// </summary>
   public partial class CompoWindow : Window {

      private int _pos;
      private List<Referee> _reftmp;
      private List<Team> _teamtmp;
      private List<Match> _matchtmp;

      private ObservableCollection<RefereeDisplayTmp> _rdisp;
      private ObservableCollection<TeamDisplayTmp> _tdisp;
      private ObservableCollection<MatchDisplayTmp> _mdisp;

      class RefereeDisplayTmp {
         public string first_name
         {
            get; set;
         }

         public string last_name
         {
            get; set;
         }

         public string spec
         {
            get; set;
         }

         public RefereeDisplayTmp(string first, string last, string spec)
         {
            this.first_name = first;
            this.last_name = last;
            this.spec = spec;
         }

      }

      class TeamDisplayTmp {
         public string teamname
         {
            get;
            set;
         }

         public int team_size
         {
            get;
            set;
         }

         public TeamDisplayTmp(string teamname, int team_size)
         {
            this.teamname = teamname;
            this.team_size = team_size;
         }

      }

      class MatchDisplayTmp {
         public string first_team
         {
            get;
            set;
         }

         public string second_team
         {
            get;
            set;
         }

         public string winner
         {
            get;
            set;
         }

         public MatchDisplayTmp(string first, string second, string winner)
         {
            this.first_team = first;
            this.second_team = second;
            this.winner = winner;
         }
      }

      public CompoWindow()
      {
         InitializeComponent();
      }

      public CompoWindow(int mgr_pos)
      {
         InitializeComponent();
         this._pos = mgr_pos;
         refreshTemp();
         refreshDisplay();
      }

      private void refreshTemp()
      {
         _reftmp = App.g_mgr.getCompetitionList()[_pos].getRefereesList();
         _teamtmp = App.g_mgr.getCompetitionList()[_pos].getTeamList();
         _matchtmp = App.g_mgr.getCompetitionList()[_pos].getMatchList();
      }


      private void refreshDisplay()
      {
         _rdisp = new ObservableCollection<RefereeDisplayTmp>();
         _tdisp = new ObservableCollection<TeamDisplayTmp>();
         _mdisp = new ObservableCollection<MatchDisplayTmp>();
         foreach (var referee in _reftmp) {
            _rdisp.Add(new RefereeDisplayTmp(referee.first_name, referee.last_name, UIUtils.specToString(referee.speciality)));
         }
         foreach (var team in _teamtmp) {
            _tdisp.Add(new TeamDisplayTmp(team.name, team.size));
         }
         foreach (var match in _matchtmp) {
            _mdisp.Add(new MatchDisplayTmp(match.getFirstTeamInfo().name, match.getSecondTeamInfo().name, UIUtils.winnerStateToString(match.getWinner())));
         }
         refereelistView.ItemsSource = _rdisp;
         teamlistView.ItemsSource = _tdisp;
         matchlistView.ItemsSource = _mdisp;
      }

      private void generatematchButton_Click(object sender, RoutedEventArgs e)
      {
         if (MessageBox.Show("All matches will be overwritten, are you sure you want to do it?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes) {
            App.g_mgr.competition_storage[_pos].prepareMatches();
         }
         refreshDisplay();
      }

      private void teamlist_MouseDoubleClick(object sender, RoutedEventArgs e)
      {
         var selidx = teamlistView.SelectedIndex;
         if (selidx != -1) {
            if (App.g_mgr.getCompetitionList()[this._pos].getTeamList().Count != 0) {
               TeamWindow w = new TeamWindow(this._pos, selidx);
               w.ShowDialog();
            } else {
               // Do nothing
            }
         }
      }


      private void matchlist_MouseDoubleClick(object sender, RoutedEventArgs e)
      {
         var selidx = matchlistView.SelectedIndex;
         if (selidx != -1) {
            if (App.g_mgr.getCompetitionList()[this._pos].getMatchList().Count != 0) {
               if (App.g_mgr.getCompetitionList()[this._pos].getCompoType() == CompetitionType.volleyball) {
                  NormalMatchWindow w = new NormalMatchWindow(App.g_mgr.getCompetitionList()[this._pos].getMatchList()[selidx]);
                  w.ShowDialog();
               } else if (App.g_mgr.getCompetitionList()[this._pos].getCompoType() == CompetitionType.linepull) {
                  NormalMatchWindow w = new NormalMatchWindow(App.g_mgr.getCompetitionList()[this._pos].getMatchList()[selidx]);
                  w.ShowDialog();
               } else if (App.g_mgr.getCompetitionList()[this._pos].getCompoType() == CompetitionType.twofires) {
                  NormalMatchWindow w = new NormalMatchWindow(App.g_mgr.getCompetitionList()[this._pos].getMatchList()[selidx]);
                  w.ShowDialog();
               }
            } else {
               // Do nothing
            }
         }
         refreshTemp();
         refreshDisplay();
      }

      private void addteamButton_Click(object sender, RoutedEventArgs e)
      {
         AddTeam w = new AddTeam();
         w.ShowDialog();
         if (w.to_add == true) {
            App.g_mgr.competition_storage[this._pos].addTeam(w.t);
         }
         refreshTemp();
         refreshDisplay();
      }

      private void addmatchButton_Click(object sender, RoutedEventArgs e)
      {
         AddMatch w = new AddMatch(App.g_mgr.competition_storage[this._pos].team_storage, App.g_mgr.competition_storage[this._pos]);
         w.ShowDialog();
         refreshTemp();
         refreshDisplay();
      }

      private void addrefereeButton_Click(object sender, RoutedEventArgs e)
      {
         AddReferee w = new AddReferee(App.g_mgr.competition_storage[this._pos]);
         w.ShowDialog();
         refreshTemp();
         refreshDisplay();
      }

      private void generateresultsbutton_Click(object sender, RoutedEventArgs e)
      {
         if (MessageBox.Show("Are you sure you want to generate results?", "Notice", MessageBoxButton.YesNo) == MessageBoxResult.Yes) {
            App.g_mgr.addResults(App.g_mgr.getCompetitionList()[_pos].generateCompoResults());
         }
      }

      private void nextstagebutton_Click(object sender, RoutedEventArgs e)
      {
         if (MessageBox.Show("Are you sure you want to generate next stage?", "Notice", MessageBoxButton.YesNo) == MessageBoxResult.Yes) {
            if (App.g_mgr.getCompetitionList()[_pos].getStageType() == StageType.eliminations) {
               App.g_mgr.getCompetitionList()[_pos].prepareForNextStage(StageType.quarterfinal);
            } else if (App.g_mgr.getCompetitionList()[_pos].getStageType() == StageType.quarterfinal) {
               App.g_mgr.getCompetitionList()[_pos].prepareForNextStage(StageType.halffinals);
            } else if (App.g_mgr.getCompetitionList()[_pos].getStageType() == StageType.halffinals) {
               App.g_mgr.getCompetitionList()[_pos].prepareForNextStage(StageType.finals);
            } else {
               MessageBox.Show("You are allready on lst stage", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            refreshTemp();
            refreshDisplay();
         }
      }
   }
}
