﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MatchHelper.Core;
using MatchHelper;

namespace MatchHelper {
   /// <summary>
   /// Interaction logic for TeamWindow.xaml
   /// </summary>
   public partial class TeamWindow : Window {

      private int _cpos;
      private int _tpos;
      private List<TeamMember> _tmtmp;
      private ObservableCollection<TeamMemberDisplayTmp> _tmdisp = new ObservableCollection<TeamMemberDisplayTmp>();

      class TeamMemberDisplayTmp {
         public string first_name
         {
            get; set;
         }

         public string last_name
         {
            get; set;
         }

         public string type
         {
            get; set;
         }

         public TeamMemberDisplayTmp(string first, string last, string type)
         {
            this.first_name = first;
            this.last_name = last;
            this.type = type;

         }

      }

      private void refreshTemp()
      {
         _tmtmp = App.g_mgr.getCompetitionList()[_cpos].getTeamList()[_tpos].getMemberList();
      }

      private void refreshDisplay()
      {
         _tmdisp = new ObservableCollection<TeamMemberDisplayTmp>();
         foreach (var member in _tmtmp) {
            _tmdisp.Add(new TeamMemberDisplayTmp(member.getFirstName(), member.getLastName(), UIUtils.teamMemberTypeToString(member.getMemberType())));
         }
         teammemberlistView.ItemsSource = _tmdisp;
      }

      public TeamWindow()
      {
         InitializeComponent();

      }

      public TeamWindow(int compopos, int teampos)
      {
         InitializeComponent();
         this._cpos = compopos;
         this._tpos = teampos;
         refreshTemp();
         refreshDisplay();
         this.teamnametextBlock.Text = App.g_mgr.getCompetitionList()[_cpos].getTeamList()[_tpos].getTeamName();
         this.teamsizetextBlock.Text = Convert.ToString(App.g_mgr.getCompetitionList()[_cpos].getTeamList()[_tpos].getTeamSize());
      }

      private void addteammemberButton_Click(object sender, RoutedEventArgs e)
      {
         AddTeamMember w = new AddTeamMember(App.g_mgr.getCompetitionList()[_cpos].getTeamList()[_tpos]);
         w.ShowDialog();
         refreshTemp();
         refreshDisplay();
      }
   }
}
